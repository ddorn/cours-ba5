\documentclass[10pt]{article}

\input{../preambule.tex}


\title{Introduction to Analytical Number Theory}
\author{Diego Dorn}

\begin{document}

\maketitle


\section{Notations}

\begin{definition}
    \begin{itemize}
        \item An arithmetic function is a function $f: \N\to\C$
        \item $\N$ are the natural numbers
        \item And $\P$ the prime numbers
        \item $[x] = \max \setst{a \in \Z }{ a <= x }$ is the integer part
        \item $\set{x}$ is the fractionnal part of $x$
        \item $\epsilon(n) = 1$ is the constant function
        \item The divisor function $\tau(n) = \#\setst{d\in\N}{d|n}$
        \item The Liouville function, $\lambda(n) = (-1)^k$ if $n = \prod p_i$.
        \item The Euler phi (totient) function : 
            $$\phi(n) = \#\setst{a\in\N}{ 1\leq a \leq n, \gcd(a, n) = 1}$$
        \item The number of distinct prime factors
            $$ \omega(n) = \sum_{d|n} 1$$
        \item The total number of prime divisor
            $$ \Omega(n) = \sum \alpha_i, \text{ if } n = \prod p_i^{a_i} $$
        \item $\Gamma(n)$ The van mangolt function defined by $\log p$ on prime 
            powers and 0 otherwise
    \end{itemize}
\end{definition}

\begin{definition}
    An arithmetic function is said to be multiplicative if $f(nm) = f(n)f(m)$ when $(m, n) = 1$.
    It is \emph{totally multiplicative} if it holds without the condition $(m, n) = 1$.

    A function is additive if $f(mn) = f(m) + f(n)$ under the same condition.
\end{definition}

The product and quotient of multiplicative functions are also multiplicative.

\begin{definition}[Moebius function]
    The Moebius function $\mu(n)$ is defined as 
    $$\mu(n) = 
    \begin{cases}
        0 & \text{if $n$ is not squarefree} \\
        (-1)^k & \text{if $n$ is the product of $k$ primes.}
    \end{cases}
    $$

\end{definition}


\begin{definition}[Dirichlet convolution]
    The \emph{Dirichlet convolution} of $f$ and $g$ is defined by
    $$ (f*g)(n) = \sum_{d|n} f(d)g\left(\frac{n}{d}\right)$$    
\end{definition}

The convolution of two multiplicative is also multiplicative.
This does not hold for complete multiplicative functions.

\paragraph{Examples}
\begin{itemize}
    \item $d(n) = \sumdn 1 = 1 * 1$
    \item $\sigma(n) = \sumdn d = \id{} * i$
    \item $\mu * 1 = e$
    \item $\mu * \id = \phi$
    \item $\sumdn \phi(d) = n$ so $\phi * 1 = \id$
\end{itemize}

One can compute the Dirichlet inverse recursively with 
$$ f^{-1}(n) = -\frac{1}{f(0)} \sum_{d|n, d\neq n} f^{-1}(d)f(n/d) $$

\paragraph{Removing coprimality condition in sums}
By a clever use of $e = 1*\mu$ and rearanging sums, one can find that, 
for a fixed $k$ and by making sure the sums converge :

\begin{align*}
 \sum_{n, (n,k)=1} f(n) 
 &= \sum_n f(n)e((n, k)) \\
 &= \sum_n f(n) \sum_{d|(n,k)} \mu(d) \\
 &= \sum_{d|k}\mu(d) \sum_{n, d|n} f(n) \\
 &= \sum_{d|k}\mu(d) \sum_m f(dm)
\end{align*}

\begin{theorem}[Möbius inversion formula]
    Let $f, g$ be arithmetic functions. Then
    $$\forall n\in\N, \ \ g(n) = \sum_{d|n} f(d)$$
    if and only if
    $$ f(n) = \mu * g = \sum_{d|n} \mu(d)g(n/d)$$
\end{theorem}

Example: we verify that $\phi*\epsilon = id$, convoluting by $\mu$ gives $\phi = id * \mu$,
so 
$$
\frac{\phi(n)}{n} = \sumdn \frac{\mu(d)}{d}
$$

\paragraph{How to solve exercices}
\begin{enumerate}
    \item To see that two multiplicative functions are equal, 
        you only need to check it for the prime powers $p^m$
    \item To compute Dirichlet products, it is often enough to evaluate it
        on prime powers and then extend the function to a multiplicative one
        (if the functions in the products are multiplicative)
    \item To prove that some function are multiplicative, try to write them also
        product or Dirichlet product of multiplicative functions. if $f*g=h$ and 
        two of them are multiplicative the third is multiplicative too.
\end{enumerate}

\section{Asymptotic estimates}

\subsection{Definitions}

\begin{definition}[Asymptotics]
    \begin{enumerate}
        \item We say that $f = o(g)$ if 
        $$\lim_{x\to\infty} \frac{f(x)}{g(x)} = 0 $$
        \item We say $f \sim g$ if 
            $$ \lim_{x\to\infty} \frac{f(x)}{g(x)} = 1 $$
        \item We say that $f = \O g$ and we write $f \ll g$ if 
            $$\exists x_0, C \in\R \st \forall x>x_0, f(x) < C\cdot g(x)$$ 
        \item If both $f \ll g$ and $g \ll f$ hold we write $f \asymp g$ or $f = \Theta(g)$.
    \end{enumerate}
\end{definition}

\begin{definition}[The logaritmic intergal]
    $$\Li(x) = \int_2^x (\log t)^{-1} dt \ \ \ (x \geq 2) $$
    It is currently the best known approximation of the prime counting function.
\end{definition}

\begin{theorem}
    For any $k \in\N$
    $$\Li(x) = \frac{x}{\log x} \left(
        \sum_{i=1}^{k-1} \frac{i!}{(\log x)^i} + \mathcal{O}_k \left(\frac{1}{(\log x)^k}\right)
    \right)$$
\end{theorem}


\begin{definition}[Riemann zeta function]
    For complex arguments $s$ with $\re s > 1$,
    $$\zeta(s) = \sum_{n=1}^\infty \frac{1}{n^s}$$
\end{definition}

\begin{theorem}[Intergal representation of Riemann zeta function]
    For $\re s > 1$,
    $$\zeta(s) = \frac{s}{s-1} - s\int_1^\infty \{x\}x^{-s-1} dx$$
\end{theorem}

\subsection{Techniques}

\begin{theorem}[Euler's sommation formula]
    Let $0 < y \leq x$ and $f(t) \in C^1([y, x])$, then
    $$ 
    \sum_{y < n \leq x} f(n) = \int_y^x f(t)dt + \int_y^x \{x\}f'(t) dt - \{x\}f(x) + \{y\}f(y)
    $$
\end{theorem}

When the sum if in the form $\sum_{n\leq x}$, one can write more simply :
$$ 
\sum_{n \leq x} f(n) = \int_1^x f(t)dt + \int_1^x \{x\}f'(t) dt - \{x\}f(x) + f(1)
$$

\begin{theorem}[Summation by parts]
    Let $a: \N\to\C$ be an arithmetic function, $0<y\leq x$ and $f \in C^1([y, x])$. 
    Then we have 
    $$
    \sum_{y<n\leq x} a(n)f(n) = A(x)f(x) - A(y)f(y) - \int_y^x A(t)f'(t)dt
    $$
    where $A(t) = \sum_{n\leq t} a(n)$.
\end{theorem}

Special case, $y = 1$ 
$$
\sum_{n\leq x} a(n)f(n) = A(n)f(n) - \int_y^x A(t)f'(t) dt
$$

\begin{theorem}[Kronecker's Lemma]
    Let $f$ be an arithmetic function, if $s$ is a complex number with $\re s > 0$
    and 
    $$ \sum_{n=1}^\infty \frac{f(n)}{n^s} \ \ \  \text{converges,} $$
    then 
    $$ \lim_{x\to\infty} \frac{1}{x^s}\sum_{n\leq x} f(n) = 0$$
\end{theorem}

In particular, with $s=1$ implises that if the sum with weights $n^{-1}$
converges, then the average of $f$ is 0

\subsection{Results}

Here is a list of useful tricks when trying to find asymptotics :
\begin{enumerate}
    \item Use the Taylor expension of functions, and if it doesn't work, 
        do it for one more order.
    \item To estimate $\int_1^N f(x)$, you can prove that $\int_1^\infty f$ exists
        and then find a big-oh for $\int_N^\infty$.
    \item 
\end{enumerate}

\begin{proposition}
    With Euler's sommation formula and the trick 2, 
    $$\sum_{n\leq x} \frac{1}{n} = \log x + \gamma + \O{1/x} $$
\end{proposition}

\begin{proposition}
    With Euler's sommation formula, Taylor of order 2 and the trick 2, 
    $$\sum_{n\leq N} \log n = N(\log N - 1) + \frac{1}{2}\log N + c + \O{1/N} $$

    In fact, $c = \log(\sqrt{2\pi})$
\end{proposition}

This can be used to deduce Stirling formula : 
$$ n! = \sqrt{2\pi n} \left(\frac{n}{e}\right)^n \left(1 + \O{\frac{1}{n}}\right)$$


\begin{proposition}
    This is an application of the Kronecker lemma. Let $f$ be an arithmetic function
    and suppose that $M(f) = \lim \frac{1}{x}\sum_{n\leq x} f(n)$ exists. Then
    $$L(f) = \lim \frac{1}{\log x} \sum_{n\leq x} \frac{f(n)}{n}$$
    exists and both are equal.
    
\end{proposition}


\end{document}
