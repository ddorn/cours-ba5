\documentclass[10pt]{article}

\input{../preambule.tex}


\title{Topics in complex analysis}
\author{Diego Dorn \\ Notes from Mathias Ruf course}

\begin{document}

\maketitle

\section{Quick recap}

\begin{definition}
    \begin{itemize}
        \item $\C$ is the complex plane
        \item $U$ is an open subset of $\C$
        \item $D$ is a domain, ie. an open and path connected subset of $\C$.
        \item $B_t(z_0) = \setst{ z\in\C }{ |z - z_0| < r}$
    \end{itemize}
\end{definition}


\begin{definition}
    A function $f: U \to \C$ is holomorphic in $z_0$ if there exists the limit
    $$f'(z_0) = \lim_{\substack{z \to 0 \\ z \neq 0}}  \frac{f(z_0 + h) - f(z_0)}{h} $$
    Also, $f$ is holomorphic on $U$ if it is holomorphic at every point of $U$.
\end{definition}

\begin{theorem}[Cauchy's integral theorem]
    Let $f: U \to \C$ be an holomorphic function and suppose that $B_r(z_0) \subset \C$.
    Then $\forall a \in \brz$,
    $$f(a) = \frac{1}{2\pi i} \int_{\partial\brz} \frac{f(x)}{(z-a)} dz$$
    where $\partial\brz$ is oriented clockwise
\end{theorem}

\begin{corollary}[Analyticity of holomorphic functions]
    Under the assumptions of the previous theorem, $f$ is analitic on $U$ and each $f^{(k)}$ is analitic
    with
    $$f^{(k)}(a) = \frac{k!}{2\pi i}\int_{\del\brz} \frac{f(z)}{(z-a)^{k+1}} dz$$
\end{corollary}

\begin{corollary}[Liouville's Theorem]
    Every bounded holomorphic function $f: \C\to\C$ is constant.
\end{corollary}

\begin{theorem}[Morera's theorem]
    Let $f: U\to\C$ be a continuous function. If for each triangle $\Delta\subset U$ it holds that
    $$\int_{\del\Delta} f(x) dx = 0$$
    then $f$ is holomorphic.
\end{theorem}

\begin{theorem}[Identity theorem]
    Let $D \subseteq \C$ be a domain, and $f, g: D\to\C$ be holomorphic. If the set
    $\setst{ z\in D }{f(z) = g(z)}$ has a an accumulation point in $D$, then $f \equiv g$
\end{theorem}

\begin{theorem}[Open mapping theorem]
    Let $D \subseteq \C$ be a domain and $f: D \to \C$ be holomorphic and non constant.
    Then $f(D)$ is again a domain.
\end{theorem}

\begin{corollary}[Maximum principle]
    Let $D \subseteq \C$ be a domain and $f: D \to \C$ be holomorphic. If $|f|$ attains its
    maximum on $D$, then $f$ is constant.
\end{corollary}

\subsection{Singularities of holomorphic functions}

\begin{definition}
    If $f: U\setminus\set{z_0}$ is holomorphic, the point $z_0$ if called
    \begin{enumerate}
        \item a removable singularity if $f$ can be extended to
              an holomorphic function $\tilde{f}: U \to\C$
        \item a pole if the exists $m\in\N$ s.t. $z\mapsto (z-z_0)^mf(z)$ has a removable
              singularity.
        \item an essential singularity if $z_0$ is neither a removable singularity or a pole.
    \end{enumerate}
\end{definition}

\begin{theorem}[Laurent series representation]

    Let $0 \leq r < R$ and $f: \setst{z\in\C}{r < |z-z_0| < R} \to \C$ be holomorphic.
    Then $f$ has the representation
    $$f(z) = \sum_{n=-\infty}^{+\infty} c_n(z-z_0)^n$$
    The term $\sum_{n=-\infty}^{-1} c_n(z-z_0)^n$ is called the principal part of $f$
    while the term $\sum_{n=0}^{+\infty} c_n(z-z_0)^n$ is the regular part of $f$.

\end{theorem}


\begin{corollary}
    Let $f:U\setminus\set{z_0} \to\C$ is holomorphic. Then $z_0$ is
    \begin{enumerate}
        \item a removable singularity iff $c_k = 0, \forall k \leq -1$ iff $f$ is bounded near $z_0$
        \item a pole of order $m$ iff $c_k  = 0,  \forall k < -m, c_{-m} \neq 0$
        \item an essential singularity iff $c_k \neq 0$ for infinitly many $k < 0$.
    \end{enumerate}
\end{corollary}




%%%%%%%%%%%
\section{Sequences of holomorphic functions}

\subsection{Local uniform convergence}

Consider a sequence $f_n: U\to\C$. We study its convergence properties.
\begin{definition}
    A sequence $f_n$ of holomorphic functions is said to converge locally uniformly to
    some $f: U\to\C$ if for each $z_0\in U, \exists r>0$ s.t.
    $$\sup_{z\in\brz} |f_n(z) - f(z)| \to 0 \text{ as } n \to \infty$$
\end{definition}

Note that local uniform convergence $\iff$ uniform convergence on each compact subset of $U$.

\begin{theorem}
    Assume that $f_n:U\to\C$ is a sequence of holomorphic functions that converges locally to some $f$.
    Then $f$ is holomorphic as well.
\end{theorem}

\proof $f$ is continuous as a locally uniform limit of continus functions.
Hence, by Morera it suffices to check that
$$ 0 = \int_{\del\Delta} f(z) dz \ \ \forall \text{ triangles } \Delta\subset U$$

Since $f_n \to f$ uniformly on $\Delta$ we conclude from Cauchy's theorem that:
$$ 0 = \lim_{n\to\infty} \int_{\del\Delta} f_n(z) dz = \int_{\del\Delta} f(z) dz $$
\endproof

\begin{theorem}
    Let $f_n: U\to\C$ be a sequence of holomorphic functions that converges locally uniformly
    to $f:U\to\C$. Then for every $k\in\N$ the sequence $f_n^{(k)}$ converges locally uniformly
    to $f^{(k)}$.
\end{theorem}

This allows us to control the numbers of zeros of limits of holomorphic functions.

\begin{corollary}
    Let $D \sub C$ be a domain and $f_n: D \to \C$ a sequence of holomorphic functions
    that converges locally uniformly to an other functio $f$.

    If each $f_n$ has at most $m$ zeros counted with multiplicity, then either $f$ is constantly zero or
    $f$ has att most $m$ zeros too.
\end{corollary}

\paragraph{Idea of proof.} Ad absurdum, we use the argument principle
$$ m < \sum_{j=1}^{l} \int_{\del B_r(z_j)} \frac{f'(z)}{f(x)} dz =
    \sum_{j=1}^{l} \lim_{n\to\infty} \int_{\del B_r(z_j)} \frac{f_n'(z)}{f_n(x)} dz \leq m $$

\begin{theorem}[Montel's theorem]
    Let $f_n: U\to\C$ be a sequence of holomorphic functions that is locally uniformly
    bounded, ie. for each $z_0\in U$ there exists $r>0$    and $C \leq +\infty$
    $$ \sup_{n\in\N} \sup_{z\in\brz} |f_n(z)| \leq C$$
    Then there exists a subsequence that converges locally uniformly for a holomorphic
    function $f$.
\end{theorem}

\paragraph{Proof idea.} Compare $|f_n(z) - f_n(z_0)|$ with Cauchy's integral fromula.

\begin{theorem}[Vitali's Theorem]
    Let $D\sub\C$ be a domain and let $f_n : D \to \C$ be a sequence pf holomorphic
    funcitons that is locally uniformly bounded. If the set
    $$L = \setst{ z\in D}{ \lim_{n\to\infty} f_n(z) \text{ exists }} $$

    has an accumulation point in $D$ then tje $f_n$ converges locally uniformly to
    some holomorphic function $f: D \to \C$.
\end{theorem}


\begin{theorem}
    Let $D$ be a domain, and let $f_n : D \to \C$ be a sequence of holomorphic functions
    that is locally uniformly bounded.

    If for all $k \in \N$ and some $z_0\in D$ the sequence of $\left( f_n^{(k)}(z_0) \right)_{n\in\N}$ converge,
    then $f_n$ converges uniformly to an holomorphic function.
\end{theorem}


\subsection{Local normal convergence}

\begin{definition}[Local normal convergence]
    Let $f_j: U\to\C$ be a sequence of complex functions.
    The series $\sum_{j=1}^{\infty} f_j$ us called \emph{locally normally convergent}
    if for each $z_0 \in U, \exists r > 0$ st.
    $$\sum_{j=1}^\infty \sup_{z\in\brz} |f_j(z)| < \infty$$

\end{definition}

\begin{lemma}
    The local normal convergence implies the local uniform convergence. In particular,
    if all $f_j$ are holomorphic on $U$ then the series is holomorphic too.
\end{lemma}


%%%%%%%
\section{The Mittag-Leffler theorem}

This theorem lets us construct functions with given singularites on an infinite but
discrete set.

\begin{definition}
    A function is called a principle part at a point if 
    its Laurent serie around this point has no regular part.
\end{definition}

\begin{theorem}[Mittag-Leffler on $\C$]
    Let $S = \set{d_n}_{n\in\N} \sub \C$ be a discrete set (ie. with no accumulation point)
    For each $d_n\in S$, let $q_n : \C\setminus\set{d_n} \to \C$ be a principal part.

    Then there exists an holomorphic $f: \C\setminus S \to C$ 
    such that at each $d_n$ the principal part of the Laurent series is $q_n$.
    The function $f$ can take this form: 

    $$ f(z_n) = \sum_{n=1}^\infty q_n(z) - p_n(z)$$

    Where $p_n$ is a polynomial and the sum converges locally normally.
\end{theorem}

\end{document}
