\documentclass[10pt]{article}

\input{../preambule}

\DeclareMathOperator{\Rad}{Rad}
\DeclareMathOperator{\diam}{diam}

\title{Graph theory}
\author{Diego Dorn}

\begin{document}
    
\maketitle

\section{Introduction}

\begin{definition}
    \begin{itemize}
        \item A graph $G$ is a pair $(V, E)$ of vertices and edges.
        \item The \emph{order} of a graph is $p = |E|$ and 
            the \emph{size} of a graph is $q = |E|$.
        \item Two graphs $(V_1, E_1)$ and $(V_2, E_2)$ are identical if 
            $V_1 = V_2$ and $E_1 = E_2$
        \item Two graphs are isomorphic if there is a map $g: V_1 \to V_2$ 
            that preserves the edges. 
        \item The degre of a vertex $v$, $\deg v$ is the number of incident edges.
        \item $v$ is an odd/even vertex if its degree is even/odd.
    \end{itemize}
\end{definition}

\begin{proposition}
    For any graph, 
    $$\sum_{v\in V} \deg v = 2q.$$

    Therefore every finite graph has an even number of odd vertices.
\end{proposition}

\begin{definition}
    A \emph{covering subgraph} is a subgraph containing all vertices.
\end{definition}

\begin{definition}
    Let $U\sub V$. A graph $(U, E')$ is \emph{vertex induced}
     if $$E' = \setst{e\in E}{e \text{ is adjacent to a vertex in } U}$$
\end{definition}

\begin{definition}
    Let $F\sub E$. A graph $(V', F)$ is \emph{edge induced} if 
    $$V' = \setst{v\in V}{v \text{ is incident to an edge in } F}$$
\end{definition}

\begin{definition}
    A graph is \emph{$s$-regular} if all the vertices have degree $s$.
\end{definition}
\begin{proposition}
    If a $(p, q)$ graph is $s$ regular, $q = \frac{ps}{2}$.
\end{proposition}

\begin{definition}[Zoo]
    \begin{itemize}
        \item $K_n$ is the complete graph on $n$ vertices
        \item $K(p_1, \dots, p_n)$ is the complete $n$-partite graph.
        \item $S_n$ is the star graph on $n$ vertices,
             with on vertex of degree $n-1$ and all other of degree 1
        \item $Q_n = K_2 \times \dots \times K_2$, $n$ times.  
        \item $C_p, p>3$ is a cycle of length $p$
        \item $P_p$ is a path with $p$ vertices and $p-1$ edges.
    \end{itemize}
\end{definition}

\begin{definition}[Operations]
    Let $G_1 = (V_1, E_1)$ and $G_2 = (V_2, E_2)$ be two graphs.
    \begin{itemize}
        \item The \emph{union} $G_1 \cup G_2$ is the union...
        \item The \emph{join} $G_1 + G_2$ is defined by $V = V_1 \cup V_2$ 
            and $$E = E_1 \cup E_2 \cup \setst{uv}{u\in V_1, v\in V_2}.$$
        \item The \emph{cartesian product} $G_1\times G_2$ is defined by 
            $V = V_1\times V_2$ and $(u_1, u_2)$ is adjacent to $(v_1, v_2)$ if either
            \begin{itemize}
                \item $u_1=v_1$ and $u_2$ is adjacent to $v_2$
                \item $u_2=v_2$ and $u_1$ is adjacent to $v_1$ 
            \end{itemize}
        \item When $V_1 = V_2$, the \emph{edge sum}, $G_1 \oplus G_2$ is 
            given by $E = E_1 \cup E_2$.
    \end{itemize}
\end{definition}

\section{Graphical sequences}

\begin{definition}[Degree/graphical sequences]
    A degree sequence is a sequence of $p$ non negative integers if it corresponds to
    the degrees of $G$ with any labelling.

    A sequence is \emph{a graphical sequence} if it is the degree sequence of a graph.
\end{definition}

\begin{theorem}[Havel-Hakimi]
    A weakly decreasing sequence $s: d_1 d_2 \dots d_p$ is a graphical sequence iff
    the sequence
    $$ d_2-1, d_3-1, \dots, d_{d_1+1}-1, d_{d_1+2}, \dots, d_p $$
    is graphical.
\end{theorem}

\section{Connectivity}

\begin{definition}[About paths]    
    A $uv$-path is :
    \begin{itemize}
        \item \emph{trivial} if it has only one vertex
        \item \emph{simple} if no edge appear twice
        \item \emph{elementary} if no vertex appear twice
        \item a \emph{circuit} or \emph{closed} if $u=v$
        \item a \emph{cycle} if $u=v$ and elementary
    \end{itemize}

    The \emph{length} of a path is the number of edges.
\end{definition}

\begin{definition}[About size of graphs]
    Let $G$ be a connected graph, $v$ a vertex:
    \begin{itemize}
        \item the \emph{eccentricity} $e(v) = \max \setst{d(v, x) }{x\in V}$ is the maximal distance to any other vertex,
        \item the \emph{radius}, $\Rad{G} = \min\setst{e(v)}{e\in V}$ is the minimum eccentricity,
        \item the \emph{diameter}, $\diam G = \max\setst{e(v)}{e\in V}$ is the maximal eccentricity,
        \item the number of connected components $k(G)$.
    \end{itemize}
    
\end{definition}

\begin{proposition}
    When $G$ is connected, we have
    $$ \Rad G \leq \diam G \leq 2\Rad G $$
\end{proposition}

\begin{proposition}
    A $(p, q)$-graph has at least $p-q$ connected components.
\end{proposition}

Reminder: $p$ is the number of vertices, $q$ the number of edges.

\begin{proposition}
    A non-trivial graph $G$ is bipartite iff it has no odd cycles.
\end{proposition}

\begin{proposition}
    Let $G$ be a self complementary graph. Then we have :
    \begin{itemize}
        \item $G$ is connected as the complement of a disconnected graph is a connected graph
        \item it does not have $K(n, m)$ as a covering subgraph.
        \item $\Rad G = 2 $
        \item $ 2 \leq \diam G \leq 3 $
        \item The order of $G$ is $4k$ or $4k+1$.
    \end{itemize}
\end{proposition}

There exists self-complementary graphs of order $4k$ and $4k+1$, 
constructed by induction on $P_4$ and $C_5$


\section{Separating vertices, bridges and blocks}

\begin{definition}[Separating edge/vertex]
    A \emph{seprating vertex} is a vertex such that $k(G-v) > k(G)$.
    A \emph{bridge} is an edge such that $k(G-e) > k(G)$
\end{definition}

\begin{proposition}
    Every non trivial graph contains at least two non-separatin vertices.
\end{proposition}
It is at least the points that realise the diameter that are non separating.

\begin{proposition}
    An edge is a bridge iff it does not lie in any cycle.
\end{proposition}

\begin{definition}[Blocks]
    A connected graph is a \emph{block} if it has no separating vertex.
    A block of a graph is a subgraph that is a block and maximal for this property.

    A \emph{final block} of $G$ is a block of a $G$ that contains only one separating vertex in $G$

    A block is \emph{minimal} if removing any edge makes it not block
\end{definition}

\begin{theorem}
    A graph of order $p \leq 3$ is a block iff every pair of vertices lies on a cycle.
\end{theorem}

\begin{theorem}
    If $G$ is connected with at least one separating vertex, it admits at least two final blocks.
\end{theorem}

The proof is done by induction and one has to note that once you pick a separating vertex 
to make blocks, it is not separating anymore in the subgraphs.

\begin{proposition}
    A minimal block of order at least 4 contains a vertex of degree 2.
\end{proposition}

\section{Planar graphs}

\subsection{Definitions}

\begin{definition}[Immersible graph]
    A $(p, q)$ graph $G$ is \emph{immersible} in a surface $S$ if there is a bijection
    between the p vertices of $G$ and $p$ points of $S$ and a bijection between the edges
    and $q$ curves of $S$ such that for each edge $e = uv$, the associated curve has $u$
    and $v$ as endpoints, and all the curves are disjoints (except at endpoints).

    A \emph{planar graph} is a graph immersible in the plane.
\end{definition}

\begin{definition}
    A region is a maximal part $R$ of the plane such that $R\setminus \cup S$ is path-connected
    for every edge curve $S$

    The \emph{boundary} or \emph{contour} of a region is the set of points $x$ that corresponds
    to vertices or edges such that for all $y\in R$ there is a path from $x$ to $y$ contained in $R$.
\end{definition}

\begin{theorem}[Euler's formula]
    A connected, planar $(p, q)$ graph with $r$ regions satisfies
    $$ p - q + r = 2 $$
\end{theorem}

If the graph is not connected, we have $p - q + r = k + 1$, where $k$ is the number of connected
components.

\begin{definition}[Maximal planar graph]
    A \emph{maximal planar graph} is a planar graph in which for each pair $u, v \in V$,
    such that $uv$ is not an edge, the graph $G + uv$ is not planar.
\end{definition}

\begin{proposition}
    A planar graph is maximal iff every region is bounded by a 3-cycle.
\end{proposition}

\begin{proposition}
    If $G$ is a $(p, q)$ graph with $p>3$, it is maximal planar iff $q = 3p-6$.
\end{proposition}

Thus, a planar graph satisfies $q \leq 3p-6$, 
it also have at least on vertex of degree 5 or less.

\begin{proposition}
    Let $G$ be maximal planar and $p_i$ the number of vertices of degree $i$. Then
    $$3p_3 + 2p_4 + p_5 = p_7 + 2p_8 + \dots + (m - 6)p_m + 12$$
    Or equivalently 
    $$ \sum (i-6)p_i = -12.$$
\end{proposition}

\subsection{Polyhedra}

\begin{definition}
    A solid is \emph{simple} or \emph{simply connected} if it is homeomorphic to a sphere. 
    A \emph{polyhedron} is a solid such that every face is a polygon and two faces are either 
    disjoints, the same or intersect in a point or a segment.
\end{definition}

The graph of a polyhedron $G(P)$ is given by the vertices and edges of the polyhedron.
It is the 1-skeleton of $P$.

\subsection{Kuratowski's Theorem}

\begin{definition}[subdivisions]
    Let $G = (V, E), uv\in E, w\notin V$, then $G' = G - uv + w + uw + vw$ is called an
    \emph{elementary subdivision}. 

    A \emph{subdivision} of $G$ is a graph obtained after a finite number of elementary subdivisions.
\end{definition}

\begin{definition}
    We say that $G$ is \textbf{isomorphic from} $H$ if $G$ is isomorphic to a subdivision of $H$

    We say that $G$ is \textbf{isomorphic with} $G'$ if there exists $H$ such that bth $G$
    and $G'$ are homeomorphic from  $H$.
\end{definition}

This is an equivalence relation. In each class there is a homeomorphically irreductible graph,
it is the graph with the minimal order.

\begin{proposition}
    A graph is homeomorphically irreductible iff it has no vertex of degree 2 on a 3-cycle.
\end{proposition}

\begin{theorem}[Kuratowski's theorem]
    A graph is not planar iff is does not contain a subgraph homeomorphic from $K_5$ or $K(3, 3)$.
\end{theorem}


\section{Methods from Linear Algebra}

\begin{definition}
    Given a $(p, q)$ graph $G$, 
    
    \begin{itemize}
        \item the \emph{adjency matrix} $A$ is a $p\times p$ matrix with 
            $$ a_{ij} = \begin{cases}
                1 & \text{if there is an edge between } v_i \text{ and } v_j \\
                0 & \text{otherwise}
            \end{cases}$$
        \item the \emph{incidence matrix}, $B \in \set{0, 1}^{p \times q}$, is defined by
            $$ b_{ij} = \begin{cases}
                1 & \text{if } v_i \text{ is incident to } e_j \\
                0 & \text{otherwise}
            \end{cases}$$
        \item the \emph{degree matrix} $C \in \Z^{p \times p}$ is given by 
            $$ c_{ij} = \begin{cases}
                deg(v_i) & \text{if } i = j \\
                0 & \text{if } i \neq j
            \end{cases}
            $$
    \end{itemize}
\end{definition}

We have that $BB^t = A + C$.

\section{Trees}

\begin{definition}
    A \emph{forest} is an acyclic graph. A \emph{tree} is a connected forest.
\end{definition}

\begin{proposition}
    A connected graph is a tree iff each of its edges is a bridge.
\end{proposition}

\begin{proposition}
    A graph $G$ is a tree iff two of the three holds
    \begin{enumerate}
        \item $G$ is connected
        \item $G$ is acyclic
        \item $q = p - 1$
    \end{enumerate}
\end{proposition}

\begin{theorem}[Kirchoff's Matrix Tree theorem]
    
\end{theorem}

\begin{theorem}[Cayley]
    The number of non identical trees on a given set of vertices is $p^{p-2}$.
\end{theorem}

For the proof, we show by a double counting argument on the ways to go from a tree 
that has $\deg v_p = d$ to $\deg v_p = d-1$ (ie. trees of type $d$ to trees of type $d-1$
that:

\begin{corollary}
    The number of non identical trees of order $p$ and type $d$ is given by 
    $$ N_d = \begin{pmatrix}
        p-2 \\
        d - 2
    \end{pmatrix} (p-1)^{p-d-1}
    $$
\end{corollary}

\section{Eulerian, Hamiltonian graphs and shortest path}

\begin{definition}[Eulerian stuff]
    In a graph $G$, a $uv$ path is an \emph{Eulerian path} that
    contains all edges exactly once.
    % if it is simple (does not repeat edges) and countains all edges of $G$.

    A closed Euler path is an \emph{Euler circuit}. If $G$ contains an Euler circuit,
    then $G$ is an \emph{Eulerian graph}.
\end{definition}

\begin{definition}[Hamiltonian stuff]
    A $uv$-path is \emph{Hamiltonian} if it 
    contains all vertices exactly once.
    % is elementary (no repeated vertices) and cover

    A graph is \emph{Hamiltonian} if it contains a Hamiltonian circuit.
\end{definition}

\begin{theorem}
    A connected non trivial graph is Eulerian iff all its vertices are even.
\end{theorem}

To find such a path, take any $uu$-circuit and add 
the circuits in the connected components created 
by removing the circuit from the graph.

\begin{proposition}
    For a connect graph $G$ with $p > 3$ if for all $u, v \in V(G)$, 
    $$\deg u + \deg v \geq p$$
    then $G$ is Hamiltonian.
\end{proposition}

\section{Coloring graphs}

\begin{definition}
    The \emph{chromatic number}, $\chi(G)$ is the minimal number of vertex colors
    such that edges are between vertecies of different colors.

    The \emph{chromatic index}, $\chi'(G)$ is the minimal number of edge colors
    such that vertices are incident to edges of different colors.
\end{definition}

\begin{proposition}
    For any graph $G$, we have 
    $$
        k \leq \chi(G) \leq \Delta(G) + 1,
    $$
    where $k$ is the size of the biggest clique in $G$ and $\Delta(G)$ the biggest degree.

    Moreover, if the graphe is non regular, $\chi(G) \leq \Delta(G)$.

\end{proposition}

\begin{proposition}
    We have 
    $$ \chi'(G) \geq \Delta(G),$$
    And the bound is tight on bipartite graphs 
    (proof by induction with swapping colors).
\end{proposition}



\section{Extremal graph theory}

\begin{theorem}[Mantel, 1907]
    If $G$ is triangle free of order $p$, then 
    $$ q \leq \left\lfloor 
        \frac{p^2}{4}
    \right\rfloor $$
\end{theorem}

The bound is obtained with the bipartite graph. 
The proof uses the fact that, forall edge $uv$, $\deg u + \deg v \leq p$
since there are no triangles and that 

\begin{theorem}[Dirac]
    If $G$ is non-Hamiltonian, of order $p \geq 3$ then $\delta(G) < \frac{p}{2}$
\end{theorem}

\subsection{Forbidden graphs: Erdös and Turán}

\begin{definition}[Turán number]
    The \emph{Turán number} of a graph $F$ (forbidden) is 
    $$\ex(p, F) = \max\setst{|E(G)|}{G \text{ is $F$-free, of order } p}$$
\end{definition}

\begin{definition}[Turán graph]
    The Turán graph $\mathcal{T}_n(p)$ is the $n$-partite complete 
    graph with vertices evenly distributed. 
    We note $t_n(p) = |E(\mathcal T_n(p))|$.
\end{definition}

We have the bounds,
$$
    \frac{n-1}{n}\begin{pmatrix}
        p \\ 
        2
    \end{pmatrix}
    \leq t_n(p)
    \leq \frac{n-1}{n} \cdot \frac{p^2}{2}.
$$

\begin{theorem}[Turán]
    For $p \geq n$, we have
    $$\ex(p, K_{n+1}) = t_n(p).$$
    And $\Turan_n(p)$ is the unique extremal graph for this problem, up to 
    isomorphism.
\end{theorem}

\begin{theorem}[Erdös]
    Let $G$ be $K_{n+1}$ free. Then there exists a $n$-partite graph $H$
    on the same vertices such that 
    $$
    \deg_H v \geq \deg_G v, \ \ \forall v \in V(G)
    $$
\end{theorem}

\begin{definition}[Turán density]
    The \emph{Turán density} or maximal edge density of a forbidden graph
    $F$ is given by 
    $$ 
    \Pi(F) = \lim_{p \to \infty} 
    \frac{\ex(p, F)}{
        \tiny \begin{pmatrix}
            p \\ 2
        \end{pmatrix}}
    $$ 
\end{definition}

\begin{lemma}
    This limit exists.
\end{lemma}

\begin{proposition}
    By Turán's theorem, we have 
    $$ \Pi(K_{n+1}) = 1 - \frac{1}{n} $$

    Also if $F$ is not $n$-partite, the Turán graph is $F$-free, so
    $\Pi(F) \geq \Pi(K_{n+1}) = 1 - \frac{1}{n}$.
\end{proposition}


\subsection{Asymptotic notations}
\begin{definition} We use
    \begin{itemize}
        \item $f \sim g$ means $\lim_{x\to\infty} f/g = 1$.
        \item $f \ll g \iff f = \mathcal O(g) \iff f(x) < cg(x)$ for all large $x$.
        \item $f \asymp g \iff f \ll g \ll f$.
    \end{itemize}
\end{definition}



\end{document}