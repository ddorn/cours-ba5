# Courses notes MA-BA5

This repository holds all my notes from the classes I've followed in the autumn
semester of 2020. They are mostly 3rd year and master classes.

The notes are all writen in LaTeX, so you need a LaTeX compiler in order to
read them in pdf. 

For each class, the notes are contained in the corresponding folder, except
for the preamble, which common to all of them and is located in the top-level
folder.
