\documentclass{article}

\input{../preambule}

\title{Lie Algebra}
\author{Diego Dorn}

\newcommand{\A}{A}
\renewcommand{\L}{L}
\newcommand{\gl}{\mathfrak{gl}}
\renewcommand{\sl}{\mathfrak{sl}}
\newcommand{\Tr}{\mathrm{Tr}}
\newcommand{\End}{\mathrm{End}}
\newcommand{\ad}{\mathrm{ad}}

\newcommand{\funcdef}[5]{ $$
    \begin{array}{ccccc}
        #1 & : & #2 & \to     & #3  \\
           &   & #4 & \mapsto & #5  \\
    \end{array}
$$ }


\begin{document}

\maketitle

\section{Définitions et exemples}

\begin{definition}
    Une \emph{algèbre} $\A$ sur $F$ est un $F$-espace vectoriel muni
    d'une application bilinéaire $\beta: \A\times\A \to \A$.
    L'algèbre est dite associative si $\beta$ est associative.
\end{definition}

\begin{definition}
    Une algèbre $\L$ est une \emph{algèbre de Lie} sur $F$
    si la multiplication que l'on note $[x, y]$ satisfait les deux conditions suivantes:
    \begin{itemize}
        \item $\forall  x\in\L, [x, x] = 0$.
        \item (identité de Jacobi) $$\forall x, y, z\in\L, [x, [y, z]] + [y, [z, x]] + [[z, [x, y]] = 0$$
    \end{itemize}
    L'algèbre de Lie est dite \emph{abélienne} si $\forall x, y\in\L, [xy] = 0$.
\end{definition}

\begin{definition}
    Quelques algèbres de Lie communes :

    \begin{itemize}
        \item $\gl_n$ est l'algèbre de lie associée à $M_{n\times n}(F)$
        \item $\gl(L) = \End_F(L)$, les applications $F$-linéaires,
              avec l'addition, compositon et le crochet induit
        \item $\sl_n = \setst{ A \in M_{n\times n}(F)}{ \Tr(A) = 0}$,
              muni du crochet $[A, B] = AB-BA$.
        \item $T_n$, les matrices triangulaires supérieures est une sous algèbre
              de Lie de $\gl_n$
    \end{itemize}
\end{definition}

\begin{definition}[Sous algèbre de Lie]
    $H \sub L$ est une sous algèbre de Lie de $L$ si
    \begin{itemize}
        \item $H$ est un sous espace vectoriel de $L$,
        \item $\forall a, b \in H$, on a $[ab] \in H$.
    \end{itemize}
\end{definition}

\begin{definition}[Idéal]
    On dit que $I\sub L$ est un idéal de $L$ si:
    \begin{itemize}
        \item $I$ est un sous espace vectoriel de $L$
        \item $\forall a\in L, \forall x \in I$ on a $[ax] \in I$
    \end{itemize}
\end{definition}


\begin{definition}[Morphismes]
    Soit $L_1$, $L_2$ des algèbres de Lie sur $F$.
    Un \emph{morphisme d'algèbre de Lie}, ou parfois \emph{homomorphisme}
    de $L_1$ dans $L_2$ est une application $F$-linéaire $\theta: L_1\to L_2$ telle que
    , pour tout $x, y \in L_1$
    $$\theta([x, y]) = [\theta(x), \theta(y)]$$

    Un \emph{isomorphisme d'algèbre de Lie} est une morphisme bijectif.
    L'inverse est aussi un isomorphisme.
\end{definition}

\begin{definition}
    Soit $A$ une $F$-algèbre.
    Une \emph{dérivation} est une application linéaire $D: A \to A$ qui
    satisfait la rêgle de Liebnitz :
    $$D(ab) = aD(b) + D(a)b$$

    L'ensemble des dérivations, $\mathrm{Der}_F(A)$ est
    une $F$-sous-algèbre de Lie
    de la $F$-algèbre de lie induite par $\mathrm{End}_F(A)$.
\end{definition}

Sur une algèbre de Lie avec multiplication et crochet, 
on peut très bien utiliser le crochet dans la définition de dérivation.
c'est d'ailleurs ce que l'on fait avec l'adjoint :

\begin{definition}[Adjoint]
    Soit $x\in L$. On définit $\ad(x) : L \to L$ par

    \funcdef{\ad(x)}{L}{L}{y}{[x, y]}

    De même, on définit \emph{la représentation adjointe} ou \emph{adjointe}
    $\ad_L$ ou $\ad$ par :

    \funcdef{\ad}{L}{\gl(L)}{x}{\ad(x)}

\end{definition}

\begin{lemma}
    Soit $L$ un $F$-algèbre de Lie.
    \begin{itemize}
        \item $\ad(x) \in \mathrm{Der}(L)$,
        \item $\ad$ est un morphisme d'algèbres de Lie.
    \end{itemize}
\end{lemma}

\begin{definition}[Quotient]
    Soit $L$ une $F$-algèbre de Lie, et $I\sub L$ un idéal.
    Alors le quotient $L/I$ est le $F$-espace vectoriel avec comme 
    classes à gauche $x+I$, muni du crochet définit par 
    $[x + I, y+I] = [x, y] + I$.
\end{definition}

La projection usuelle $\pi : L \to L/i$ définit bien un morphisme de noyau $I$.

\begin{theorem}
    Soit $\phi : L_1 \to L_2$ un morphisme d'algèbre de Lie,
    $I \sub \ker(\phi)$ un idéal de $L_1$, alors il existe un 
    unique morphisme $\hat{\phi} : L_1/I \to L_2$ tel que
    $\hat{\phi} \circ \pi = \phi$. En gros, ce diagramme commute :

    \begin{center}
        \begin{tikzcd}
            L \arrow{rr}{\phi} \arrow[swap]{dr}{\pi} & & B \\
            & L/I \arrow[swap, dashed]{ur}{\hat{\phi}}
        \end{tikzcd}
    \end{center}

    Et ce morphisme est un isomorphisme.

\end{theorem}

\begin{theorem}
    Soit $L$ une algèbre de Lie, $H\sub L$ une sous-algèbre de Lie, et 
    $I\sub J \sub L$ des idéaux. Alors 
    \begin{enumerate}
        \item $H+I$ est une sous algèbre de Lie de $L$, $H\cap I$ est un idéal de $H$
            et $I$ est un idéal de $H+I$.
        \item On a un isomorphisme $^{H+I}/_I \cong {} ^H/_{H\cap I}$
        \item Il existe une bijection 
            $$ \setst{I \sub M \sub L}{M \text{ une sous-algèbre}} \leftrightarrow \set{ \text{sous algèbres de L/I}}$$
            Donnée par $M \mapsto M/I$, et cette bijection preserve les idéaux.
    \end{enumerate}
\end{theorem}

\end{document}