\documentclass{article}

\input{../preambule}

\title{Théorie de Galois}
\author{
    Notes par Diego Dorn \\
    du cours de Thomas Gerber
}


\begin{document}
    \maketitle

    \setcounter{section}{-1}
    \section{Rappels}

    \begin{proposition}
        Résultats d'anneaux et corps:
        \begin{itemize}
            \item Le noyau et l'image d'un morphisme d'anneau est un idéal.
            \item Si $I$ est un idéal maximal $A/I$ est un corps.
            \item Si $I$ est un idéal premier $A/I$ est intègre.
            \item $A/\ker f$ est intègre $\iff \im f$ est intègre.
            \item $K[X]$ est principal $\iff K$ est un corps.
            \item Euclidien $\rightarrow$ Principal $\rightarrow$ Factoriel
        \end{itemize}
    \end{proposition}

    \begin{definition}
        Un polynôme est \emph{primitif} si son \emph{contenu} ie. le pgdc de ses 
        coefficients est 1. Dans $K[x]$, tous les polynômes sont primitifs, donc on peut 
        ignorer cette condition pour les critères.
    \end{definition}

    \begin{proposition}[Critère d'irréductibilité]
        Soit $f(x) = a_0 + a_1x + \dots + a_nx^n$, et $p \in A$ irréductible.
        \begin{itemize}
            \item Si $f$ est unitaire (ou primitif et $p \nmid a_n$), 
                alors $f$ irréductible $\iff$ la réduction modulo $p$ de $f$ est irréductible. 
            \item Gauss: $f$ irréductible dans $K[x] \iff f$ irréductible dans $K(x)$.
            \item Eisenstein:
                Si $p \nmid a_n$, $\forall i < n, p | a_i$ et $p^2 \nmid a_0$ et $f$ est primitif,
                alors $f$ est irréductible dans $K[x]$
        \end{itemize}

    \end{proposition}

    \begin{proposition}[Trigonometrie]
        On a 
        \begin{itemize}
            \item $\cos 2x = 2\cos^2 x - 1$
            \item $\cos 3x = 4\cos^3 x - 3\cos x$
        \end{itemize}
    \end{proposition}


    
    \section{Extension de corps}
    \subsection{Extensions et degrés}

    \begin{definition}
        Un \emph{corps} est un anneau commutatif unitaire ($1 \neq 0$) 
        tel que tout élément différent de 0 admet un inverse.
        Un \emph{morphisme de corps} est un morphisme d'anneau entre
        deux corps.
    \end{definition}

    \begin{proposition}
        Tout morphisme de corps est injectif.
    \end{proposition}

    \begin{definition}
        Un extension d'un corps $K$ est la donnée d'une corps $L$ et d'un morphisme
        $K \to L$, on le note $L/K$. Une \emph{tour d'extension} est une suite 
        finie d'extensions de corps.

        Si on a $L/K$, alors $L$ est une $K$-algèbre, et donc un espace vectoriel.
    \end{definition}

    \begin{definition}[Sous-corps premier]
        L'intersection de tous les sous-corps de $K$ est appellé le \emph{sous-corps premier}
        de $K$ et est noté $\Pi(K)$.
    \end{definition}

    \begin{definition}[Caractéristique]
        Pour tout $K$, on a $\Pi(K) \simeq \Q$ ou $\Pi(K) \simeq \F_p$ 
        (en regardant l'inclusion de $\Z$). On définit la \emph{caractéristique}
        d'un corps par 0 si $\Pi(K) \simeq \Q$ et par $p$ sinon.
    \end{definition}

    \begin{lemma}[Extensions générés par des elements]
        Soit $L/K$ un extension de corps et $A \subset L$. On note 
        \begin{itemize}
            \item $K(A)$ le plus petit (intersection) sous corps de $L$ qui contient $A$.
            \item $K[A]$ le plus petit (intersection) sous anneau de $L$ qui contient $A$.
        \end{itemize}

        Si $A$ est fini, on dit que $K(A)/K$ est \emph{de type fini} et 
        si $A = \set{a}$ que $K(a)/K$ est \emph{monogène}.
    \end{lemma}

    % Pour $\alpha \in K(A)$, on peut écrire $\alpha = \sum_{i=1}^{n} k_i \alpha^i$, 
    % pour $k_i \in K$ et $\alpha \in A$
    
    \begin{definition}[Degré d'un extension]
        Soit $L/K$ un extension de corps.
        \begin{itemize}
            \item Le \emph{degré} de $L/K$ est la dimmension de
                $L$ en tant que $K$-ev entre noté $[L : K]$.
            \item Une extension est dite \emph{finie} si son degré est fini 
                et sinon \emph{infinie}.
            \item Un extension de degrée 2 est dite $quadratique$.
        \end{itemize}
    \end{definition}

    \begin{theorem}[de la base téléscopique]
        Soient $L/K$ et $M/L$ deux extensions de corps,
        $\set{e_i}$ une base de $L$ sur $K$
        et 
        $\set{f_i}$ une base de $M$ sur $L$. 
        Alors $\set{e_if_j}$ est une base de $M$ sur $K$.

        On a donc 
        $$ [M:K] = [M:L][L:K] $$

    \end{theorem}


    
    \subsection{Extensions monogènes}

    Soit $L/K$ une extension de corps, et $a \in L$

    \begin{definition}[Algébrique et transcendant]
        Soit $a \in L$. On dit que $a$ est 
        \begin{itemize}
            \item \emph{algébrique} sur $K$ s'il annule un polynôme $f \in K[X]\setminus {0}$ 
            \item \emph{transcendent} sinon. On a alors $K(a) \simeq K(X)$.
        \end{itemize}
        De même une extension est 
        \begin{itemize}
            \item \emph{algébrique} si tous ses éléments sont algébriques
            \item \emph{transcendante} sinon.
        \end{itemize}
    \end{definition}

    \begin{definition}[Polynome minimal]
        Le \emph{polynôme minimal} de $a$ est $p \in K[X]$ unitaire, 
        tel que $p(a) = 0$ et, au choix
        \begin{itemize}
            \item $p$ de degré minimal.
            \item $p$ divise tout $h$ tel que $h(a) = 0$.
            \item $p$ est irréductible.
        \end{itemize}
    \end{definition}

    \begin{theorem}
        Soit $f$ le polynôme minimal de $a$, de degré $n$, alors
        \begin{itemize}
            \item $K[a] = K(a) \simeq K[X] / (f)$
            \item $[K(a) : K] = n$ et $\set{1, a, a^2, \dots a^{n-1}}$ 
                est une $K$-base de $K(a)$. 
        \end{itemize}
    \end{theorem}

    \begin{theorem}
        Soit $K$ une extension quadratique de $\Q$, alors $K = \Q(\sqrt{d})$, pour 
        $d \in \Z \setminus \set{0, 1} $ sans facteur carré.
    \end{theorem}

    \subsection{Extensions algébriques}

    \begin{theorem}
        Toute extension finie est algébrique, de plus 
        $$ K/L \text{ finie} \iff L = K[a_1, \dots, a_n]$$ 
        où $a_1, \dots, a_n$ sont algébriques sur K.
    \end{theorem}

    \begin{corollary}
        \ 
        \begin{itemize}
            \item Soit $A \subset L$. $K(A)/K$ est algébrique ssi tout $a \in A$ est 
                algébrique sur $K$. (c'est biend dans $A$ et pas $K(A)$).
            \item $\tilde{K} = \setst{a \in L}{a \text{ est algébrique sur } K}$ est
                un sous-corps de de $K$. C'est la \emph{fermeture algébrique} de $K$ dans $L$.
            \item Soit $M/L/K$ une tour d'extension, $M/K$ est algébrique ssi $M/L$
                et $L/K$ sont algébriques.
        \end{itemize}
    \end{corollary}

    Pour le dernier point, il faut regarder l'extension finie générée par les coefficients
    du polynôme anulateur.

    \section{Construction à la règle et au compas}

    \subsection{Points constructibles}
    Soit $E$ un ensemble de points du plan. 
    \begin{itemize}
        \item $D_E$ est l'ensemble des droites passants par deux points de $E$
        \item $C_E$ est l'ensemble des cercles de centre un point de $E$ et de 
            rayon la distance entre deux points de $E$.
        \item Un point est constructible si c'est l'intersection entre deux éléments
            de $D_E \cup C_E$, ou une itération de ce processus
    \end{itemize}

    \subsection{Nombres constructibles}

    On prend $E = \set{0, 1}$

    \begin{proposition}
        On a
        \begin{itemize}
            \item Un nombre est constructible si le point $(0, a)$ est constructible.
            \item Tout rationnel est constructible.
            \item Les constructibles font un sous-corps de $\R$.
            \item Si $a > 0$ est constructible, $\sqrt{a}$ est constructible.
        \end{itemize}
    \end{proposition}

    \begin{proposition}
        Soit $F$ un ensemble de points constructibles et $K = \Q(F)$,
        \begin{itemize}
            \item Si $(a, b)$ est constructible en une étape, soit $a, b, \in K$,
                soit $a, b$ sont dans une extension quadratique de $K$.
            \item Tout élément d'une extension quadratique de $K$ est constructible.
        \end{itemize}
    \end{proposition}

    \begin{theorem}[Wantzel]
        Soit $a \in \R$. $a$ est constructible ssi 
        il existe une tour d'extensions \emph{quadratiques} :
        $$ \Q = K_0 \subseteq K_1 \subseteq \dots \subseteq K_n \subseteq \R $$
        telles que $a \in K_n$.
    \end{theorem}

    \begin{corollary}
        Si $a$ est constructible $[\Q(a) : \Q]$ est une puissance de 2.
    \end{corollary}

    \begin{corollary}
        Rip les grecs.
    \end{corollary}



    \section{Extension normales et séparables}

    \subsection{Corps de décomposition et de rupture}
    \begin{definition}[Corps de rupture]
        $L/K$ est un \emph{corps de rupture} de $f$ sur $K$
        si il existe $a \in L$ tq $f(a) = 0$ et $L = K(a)$.
    \end{definition}
    
    \begin{lemma}[Prolongement des isomorphismes]
        cf p21 du cours.
    \end{lemma}

    \begin{theorem}
        Soit $f \in K[x]$ irréductible,
        \begin{itemize}
            \item Il existe un corps de rupture de $f$ sur $K$.
            \item Soit $K(a)$ et $K(a')$ deux corps de rupture de $f$, il existe
                un unique isomorphisme $\tau : K(a) \to K(a')$ tel que $\tau_{|K} = \id$
                et $\tau(a) = a'$
        \end{itemize}
    \end{theorem}

    \begin{definition}[Corps de décomposition]
        Un extension $L/K$ est un \emph{corps de décomposition}
        de $f$ sur $K$ si il existe $c, a_1, \dots, a_n \in L$ tels que
         $f = c \prod_{i=1}^{n}(X - a_i)$
        et $L = K(a_i, \dots, a_n)$.
    \end{definition}

    \begin{lemma}
        Soit $f \in K[x]$. Si
        \begin{itemize}
            \item $\sigma : K \to K'$ est un isomorphisme
            \item $L = K(a_1, \dots, a_n)$ est un corps de décomposition de $f$
            \item $L' = K'(a'_1, \dots, a'_n)$ est un corps de décomposition de $\sigma(f)$
        \end{itemize}
        alors, on a un isomorphisme $\tau: L \to L'$ qui envoie les $a_i$ sur $a'_i$,
        possiblement en les permutant.
    \end{lemma}

    \begin{theorem}
        Soit $f \in K[x]$ et $n = \deg f$.
        \begin{itemize}
            \item Il existe un corps de décomposition $L$ de $f$ sur $K$
            \item $[L : K] \leq n!$ 
            \item Deux corps de décomposition de $K$ sont isomorphes 
                et l'isomorphisme permute les racines.
        \end{itemize}
    \end{theorem}

    \begin{definition}[K-morphismes]
        Soit $L/K$ et $L'/K$ deux extensions. 
        \begin{itemize}
            \item Un morphisme $\sigma : L \to L'$
                est un \emph{$K$-morphisme} si $\sigma|_{K} = \id$. 
            \item $\Hom(L/K, L'/K)$ est l'ensemble des $K$-morphismes.
            \item $\Aut(L/K)$ l'ensemble des $K$-automorphismes.
        \end{itemize}
    \end{definition}

    \subsection{Clôture algébrique}

    \begin{proposition}
        Soit $K$ un corps. Les 5 points sont équivalents.
        \begin{itemize}
            \item $K$ est algébriquement clos.
            \item Tout polynôme non constant sur $K$ est sindé.
            \item Tout polynôme non constant sur $K$ admet un racine dans $K$.
            \item Les seuls polynôme irréductibles de $K[x]$ sont de degré 1.
            \item Toute extension algébrique de $K$ est égale à $K$.
        \end{itemize}
    \end{proposition}

    \begin{lemma}
        Soit $L/K$ un extension, $M$ algébriquement clos et $\sigma : K \to M$.
        Alors, 
        \begin{itemize}
            \item Pour $a \in L$, on peut prolonger $\sigma$ de $K(a) \to M$.
            \item Si $L$ est algébrique, on peut prolonger $\sigma$ de $L \to M$.
        \end{itemize}
    \end{lemma}

    \begin{definition}[Clôture algébrique]
        Soit $L/K$ un extension. $L$ est une \emph{clôture algébrique} de $K$
        si $L$ est algébriquement clos et $L/K$ est algébrique.
    \end{definition}

    \begin{theorem}[Steiniz]
        Il existe une clôture algébrique de $K$, et deux clôtures sont $K$-isomorphes.
    \end{theorem}

    \subsection{Extensions normales}

    \begin{definition}[Extension normale]
        Une extension $K/L$ est \emph{normale} si elle est algébrique
        et que tout polynôme irréductible de $K[x]$ qui a une racine dans $L$
        scinde dans $L$.
    \end{definition}
    Ainsi les polynômes irréductibles de $K[x]$ ont soit toutes leurs racines soit
    aucunes dans $L$.

    \begin{theorem}
        Soit $L/K$ une extension finie. $L/K$ est normale ssi 
        il existe $f \in K[x] \setminus K$ tel que $L$ est un corps de décomposition
        de $f$ sur $L$.
    \end{theorem}

    \begin{corollary}
        Soit $M/L/K$ une tour d'extension, telle que $M/K$ est normale et finie.
        \begin{itemize}
            \item Alors $M/L$ est normale
            \item Pour tout $K$-morphisme $\tau : L \to M$, il existe une $K$-automorphisme
                $\sigma : M \to M$ qui prolonge $\tau$.
        \end{itemize}
    \end{corollary}

    \begin{definition}[Clôture normale]
        Soit $L/K$ une extension algébrique. Une \emph{clôture normale}
        de  $L/K$ est une extension $M$ telle que 
        \begin{itemize}
            \item $M/K$ est normale.
            \item $M$ est minimale pour cette propriété au sens de l'inclusion.
                ie. $L \subset M' \subseteq M$ avec $M'$ normale $\Rightarrow M = M'$.
        \end{itemize}
    \end{definition}

    \begin{theorem}
        Soit $L/K$ finie.
        \begin{itemize}
            \item Il existe une clôture normale $M$ de $L/K$ et $M$ est finie.
            \item Deux clôtures normales sont $K$-isomorphes.
        \end{itemize}
    \end{theorem}

    \begin{theorem}
        Soit $L/K$ une extension finie, alors $L/K$ est normale ssi pour toute extension $M/K$
        contenant $L$ et pour tout $K$-morphisme $\sigma : L \to M$, $\sigma(L) = L$.
    \end{theorem}

    \begin{theorem}
        Soit $L/K$ une extension:
        \begin{itemize}
            \item si $L/K$ est normale alors pour toute extension $M/K$
                contenant $L$ et pour tout $K$-morphisme 
                $\sigma : L \to M$, $\sigma(L) \subseteq L$.
            \item si pour tout $K$-morphisme $\sigma : L \to M$, $\sigma(L) \subseteq L$
                avec $M/L$ normale, alors $L/K$ est normale.
            \item Si $L/K$ est finie, alors les $\subseteq$ sont des $=$.
    
        \end{itemize}
    \end{theorem}

    \subsection{Extensions séparables}

    \begin{definition}[Dérivée, racines]
        Soit $f = \sum_{i=0}^n a_ix^i \in K[x]$ 
        \begin{itemize}
            \item La \emph{dérivée} de $f$ est $f' = \sum_{i=1}^n ia_ix^{i-1}$.
            \item On a $(fg)' = f'g + fg'$.
            \item Une racine $a$ est \emph{multiple} si $(x-a)^2$ divise $f$.
            \item Sinon la racine est \emph{simple}
        \end{itemize}
    \end{definition}

    \begin{proposition}
        Soit $a \in L$ une racine de $f \in K[x] \setminus K$.
        \begin{itemize}
            \item $a$ est une racine multiple de $f$ dans $L$ ssi $f'(a) = 0$.
            \item Si de plus $f$ est irréductible, $a$ est une racine muliple ssi $f' = 0$.
        \end{itemize}
    \end{proposition}

    \begin{definition}[Séparable, parfait]
        Soit $f \in K[x]\setminus K$, et $L/K$ une extension.
        \begin{itemize}
            \item  On dit que $f$ est \emph{séparable}
                si $f$ n'a que des racines simples
                dans son corps de décomposition.
            \item $a \in L$ est \emph{séparable} sur $K$ si son polynôme minimal 
                sur $K$ est séparable.
            \item Une extension est \emph{séparable} si tous $a \in L$ est séparable sur $K$
            \item Un corps est \emph{parfait} si toute extension algébrique est séparable.
        \end{itemize}
    \end{definition}

    Un corps est parfait si tous les polynômes minimaux n'ont que des racines simples.

    \begin{proposition}
        Dans un corps de caractéristique nulle, tout polynôme irréductible 
        est séparable.
    \end{proposition}

    \begin{proposition}
        On a des équivalences:
        \begin{itemize}
            \item $K$ est parfait
            \item Tout polynôme irréductible de $K[x]$ est séparable
            \item $\overline{K} / K$ est séparable
        \end{itemize}
    \end{proposition}

    \begin{proposition}
        $K$ est parfait ssi 
        \begin{itemize}
            \item $\Char(K) = 0$ ou
            \item $\Char(K) = p$ et $K = K^p$ (ie. $x \mapsto x^p$ est surjectif)
        \end{itemize}
    \end{proposition}

    \begin{theorem}[de l'élément primitif]
        Soit $L/K$ une extension séparable finie. Alors il existe $a \in L$ tel
        que $L = K(a)$. On dit que $a$ est un \emph{élément primitif}.
    \end{theorem}

    \begin{lemma}
        Soit $K \subseteq L \subseteq M\subseteq N$ une tour d'extension avec $N/K$ 
        normale et finie. Alors
        \begin{itemize}
            \item $|\Hom(L/K, N/K)| < \infty$
            \item $|\Hom(M/K, N/K)| = |\Hom(M/K, N/L)| \cdot |\Hom(L/K, N/K)|$
        \end{itemize}
    \end{lemma}

    \begin{theorem}
        Soit $K \subseteq L \subseteq N$ une tour d'extension avec $N/K$ 
        normale finie et $a \in L$
        \begin{itemize}
            \item $|\Hom(L/K, N/K)| \leq [L : K]$ 
            \item On a égalité ssi $L/K$ est séparable.
            \item Si $L = K(a)$, on a égalité ssi $a$ est séparable.
        \end{itemize}
    \end{theorem}

    \begin{corollary}
        Soit $L/K$ finie.
        \begin{itemize}
            \item Soit $K \subseteq K' \subseteq L$ une tour d'extension.
                $$ L/K \text{ séparable } \iff L/K' \text{ et } K'/K \text{ séparable} $$
            \item Soit $L = K(a_i, \dots, a_n)$ avec $a_i$ algébriques sur $K$, alors
                $$ L/K \text{ séparable } \iff a_1, \dots, a_n \text{ séparable} $$
            \item L'ensemble $\setst{a \in L}{ a \text{ est séparable sur } K}$
                est un sous corps de $L$ appelé la \emph{cloture séparable} de $K$ dans $L$
        \end{itemize}
    \end{corollary}
    

    \section{Théorie de Galois}
    \subsection{Groupe de Galois et extentions galoisiennes}

    \begin{definition}[Groupe de Galois]
        L'ensemble $\Aut(L/K)$ est un groupe, que l'on nomme le \emph{groupe 
        de Galois} de $L/K$ et noté $\Gal(L/K)$.

        On not aussi $\Aut(K) = \Aut(K/\Pi(K))$.
    \end{definition}

    \begin{definition}[Extension galoisienne]
        Une extension est dite \emph{galoisienne} si elle est normale et 
        séparable.
    \end{definition}

    C'est-à-dire que tout élément algébrique a tous ses jumeaux et en a le bon nombre.

    \begin{definition}[Corps fixe]
        Soit $G \subseteq \Aut(L)$, alors 
        $$ L^G = \setst{a \in L}{\sigma(a) = a, \ \forall \sigma \in L} $$
        est appellé le \emph{corps fixe} de $G$.
    \end{definition}

    \begin{theorem}
        Soit $L/K$ algébrique, et $G = \Gal(L/K)$. Alors $L/K$ est galoisienne ssi 
        $L^G = K$.
    \end{theorem}

    \begin{theorem}
        Soit $L$ un corps et $H \leq \Aut(L)$ un sous groupe fini. Alors $L/L^H$ est une
        extension galoisienne finie, $[L : L^H] = |H|$ et $H = \Gal(L/L^H)$.
    \end{theorem}

    \begin{corollary}
        Soit $L/K$ une extension finie, alors $L/K$ est galoisienne 
        ssi $[L : K] = |\Gal(L/K)|$.
    \end{corollary}

    \subsection{La correspondance de Galois}

    Soit $L/K$ une exxtension finie. 
    $$
        \calG = \set{\text{sous-groupes de } \Gal(L/K)}
        \quad \text{et} \quad
        \calK = \set{\text{sous-corps de $L$ contenant $K$}},
    $$
    ainsi que 
    
    $$
    \func{\Phi}{\calG}{\calK} {H}{L^H}
        \qquad \text{et} \qquad
    \func{\Psi}{\calK}{\calG} {K}{\Gal(L/K)}
    $$

    \newpage
    \begin{theorem}[Correspondance de Galois]
        Soit $L/K$ une extension galoisienne finie.
        \begin{enumerate}
            \item $\Phi$ et $\Psi$ sont bijectives et inverses l'une de l'autre, c'est
                la \emph{correspondance de Galois}
            \item Soit $G = \Gal(L/K)$.
                \begin{enumerate}
                    \item Pour $H_1, H_2 \in \calG$, 
                        $$ H_1 \leq H_2 \iffquad L^{H_2} \subseteq L^{H_1} $$

                    \item Pour $M_1, M_2 \in \calK$,
                        $$ M_1 \subseteq M_2 \iffquad \Gal(L/M_2) \leq \Gal(L/M_1) $$
                    \item Pour $M \in \calK$ et $\sigma \in G$,
                        $$ \sigma(M) \in \calK 
                            \ \et\ 
                           \Gal(L/\sigma(M)) = \sigma\Gal(L/M)\sigma^{-1} $$
                    \item Pour $M \in \calK$, 
                        $$ M/K \text{ normale } \iffquad \Gal(L/M) 
                            \text{ est un sous groupe normal de } G $$
                    \item Pour $M \in \calK$ avec $M/K$ normale,
                        $$ \Gal(M/K) = G / \Gal(L/M) $$
                \end{enumerate}
        \end{enumerate}
    \end{theorem}

    \begin{definition}
        Soit $f \in K[x]\setminus K$. 
        Le \emph{groupe de Galois} de $f$ est $\Gal(f) = \Gal(L/K)$ où $L$ est le 
        corps de décomposition de $f$ sur $K$.
    \end{definition}

    \begin{theorem}
        Soit $K$ un corps et $f \in K[x]\setminus K$ séparable,
        $L$ le corps de décomposition de $f$ sur $K$ et 
        $Z$ l'ensemble des racines de $f$ dans $L$. Ainsi $L = K(Z)$ et 
        \begin{itemize}
            \item $L/K$ est galoisienne finie.
            \item $\Gal(L/K)$ agit fidèlement sur $S_Z$, càd que l'identité est le seul 
                morphisme qui fixe toutes les racines.
            \item $\Gal(L/Z)$ agit transitivement sur $S_Z$ ssi $f$ est irréductible sur $K$.
        \end{itemize}
    \end{theorem}

    \subsection{Polynômes symétriques}

    $S_n$ agit sur $K[x_1, \dots, x_n]$ en permutant les racines.

    \begin{definition}
        Un élément $f \in \Kxx]$ est un \emph{polynôme symétrique}
        si pour tout $\sigma \in S_n$, $\sigma f = f$. L'ensemble des polynômes
        symétriques est un sous-anneau de $\Kxx$ noté $\Kxx^{S_n}$.
    \end{definition}

    \begin{definition}
        Les polynômes symétriques élémentaires sont
        $$ s_1 = \sum_{1 \leq i \leq n} X_i $$
        $$ s_2 = \sum_{1 \leq i < j \leq n} X_iX_j $$
        $$\dots$$
        $$ s_3 = X_1X_2\cdots X_n $$
    \end{definition}

    \begin{proposition}
        Si $f = X^n + c_1X^{n-1} + \dots + c_n$, et $a_1, \dots, a_n$ les racines de $f$,
        alors $c_i = (-1)^is_i(a_1, \dots, a_n)$.
    \end{proposition}

    \begin{theorem}[fondamental des fonctions symétriques]
        Soit $f \in \Kxx^{S_n}$, alors il existe un unique $g \in \Kxx$ tel que
        $f = g(s_1, \dots, s_n)$.
    \end{theorem}

    C'est à dire que les $s_i$ engendrent algébriquement $\Kxx^{S_n}$ et sont indépendants,
    si bien que
    $$ \Kxx^{S_n} = K[s_1, \dots, s_n].$$


    \begin{theorem}
        On a
        \begin{itemize}
            \item $K(x_1, \dots, x_n)^{S_n} = K(s_1, \dots, s_n)$
            \item $K(x_1, \dots, x_n)/K(s_1, \dots, s_n)$ est galoisienne, de groupe
                de galois $S_n$.
        \end{itemize}
    \end{theorem}

    \subsection{Le discriminant}

    D'après le théorème fondamental, on a $\Delta^{(n)}$ tel que
    $$ \Delta^{(n)}(s_1, \dots, s_n) = \prod_{1 \leq i < j \leq n} (X_i - X_j)^2
    \in \Z[X_1, \dots, X_n]$$

    \begin{definition}[Discriminant]
        Soit $f = X^n + c_1X^{n-1} + \dots + c_n \in K[X]$, son \emph{discriminant}
        est 
            $$ \Delta_f = \Delta^{(n)}(-c_1, c_2, \dots, (-1)^n c_n) \in K$$
    \end{definition}

    On peut noter que:
    \begin{itemize}
        \item $\Delta^{(2)} = X_1^2 - 4X_2$
        \item $\Delta^{(3)} = X_1^2X_2^2 - 4X_2^3 - 4X_1^3X_3 - 27X_3^2 + 18X_1X_2X_3$
        \item Pour le degré 3, si $c_1 = 0$, on a $\Delta_f = -4c_2^3 - 27c_3^2$. 
        \item Pour le degré 4, si $c_1 = 0$, on a $\Delta_f = 16c_2^4c_4 - 4c_2^3c_3^2
            -128c_2^2c_4^2 +144c_2c_3^2c_4 -27c_3^2 + 256c_4^3$.
    \end{itemize} 

    \begin{proposition}
        Soit $f \in K[x]$ et $L$ le corps de décomposition de $f$ sur $K$.
        Soient $a_1, \dots, a_n$ les racines de $f$ dans $L$. Alors,
        $$\Delta_f = \prod_{1 \leq i < j \leq n} (a_i - a_j)^2$$

        En particulier $\Delta_f \neq 0 \iff f$ est séparable. 
    \end{proposition}

    \begin{theorem}
        Soit $K$ in corps avec $\Char(K) \neq 2$ et $f \in K[x]$ unitaire, séparable de 
        degré $n$, et $L$ son corps de décomposition.
        \begin{itemize}
            \item $\Delta_f$ est un carré dans $L$.
            \item $\Delta_f$ est un carré dans $K$ ssi $\Gal(f)$ est isomorphe 
                à un sous groupe de $A_n$.
        \end{itemize}
    \end{theorem}
    
    \begin{corollary}
        Soit $\Char(K) \neq 2$, et $f$ irréductible, unitaire et séparable de degré 3,
        alors

        $$
            \Gal(f) = 
                \begin{cases}
                    A_3     & \text{si $\Delta_f$ est un carré dans } K, \\
                    S_3     & \text{sinon.}
                \end{cases}
        $$
    \end{corollary}

    \begin{proposition}
        Les sous groupes transitifs de $S_4$ sont $S_4, A_4, V, 
        \angles{\sigma} \simeq C_4, \angles{V, \sigma} \simeq D_4$. 
        Où $\sigma$ est un 4-cycle et $V$ est le groupe de Klein ($C_2\times C_2$)
    \end{proposition}

    \begin{theorem}
        Pour le degré 4, regarder dans le poly du cours, page 49.
    \end{theorem}





    \newpage
    \  
    \newpage
    $x \rhd\hspace{-0.9em}\setminus\ y$
\end{document}