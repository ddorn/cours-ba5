{ pkgs ? import <nixpkgs> {} }:

(pkgs.buildFHSUserEnv {
  name = "vscodium-env";
  targetPkgs = pkgs: with pkgs; [
    dotnetCorePackages.netcore_3_1 icu patchelf gcc gcr liburcu openssl krb5 zlib gnome3.gnome-keyring libsecret desktop-file-utils xorg.xprop
    texlive.combined.scheme-full
    vscodium

    libGL

    pkgs.mesa
    pkgs.mtdev

    pkgs.SDL2
    pkgs.SDL2_image
    pkgs.SDL2_ttf
    pkgs.SDL2_mixer

    # NOTE: The degree to which gstreamer actually works is unclear
    pkgs.gst_all_1.gstreamer
    pkgs.gst_all_1.gst-plugins-base
    pkgs.gst_all_1.gst-plugins-good
    pkgs.gst_all_1.gst-plugins-bad

  ];
  runScript = ''poetry shell'';
}).env
